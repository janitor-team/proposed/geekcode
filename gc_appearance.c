/*
 gc_appearance.c - Functions for the Appearance section

 Geek Code Generator v1.7.3 - Generates your geek code
 Copyright (C) 1999-2003 Chris Gushue <chris@blackplasma.net>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <stdio.h>
#include "geekcode.h"

int get_dress(void)
{
   int selection = 99;

   do
     {
	clearscreen();
	printf("Dress                                                              Page %i of %i\n", PAGES-(PAGES-2), PAGES);
	printf("===============================================================================\n");
        printf(" 1 d++  I tend to wear conservative dress such as a business suit or\n");
        printf("        worse, a tie.\n");
        printf(" 2 d+   Good leisure-wear. Slacks, button-shirt, etc. No jeans, tennis\n");
        printf("        shoes, or t-shirts.\n");
        printf(" 3 d    I dress a lot like those found in catalog ads. Bland, boring,\n");
        printf("        without life or meaning.\n");
        printf(" 4 d-   I'm usually in jeans and a t-shirt.\n");
        printf(" 5 d--  My t-shirts go a step further and have a trendy political\n");
        printf("        message on them.\n");
        printf(" 6 d--- Punk dresser, including, but not limited to, torn jeans and\n");
        printf("        shirts, body piercings, and prominent tattoos.\n");
        printf(" 7 dx   Cross dresser.\n");
        printf(" 8 d?   I have no idea what I am wearing now, let alone what I wore yesterday.\n");
        printf(" 9 !d   No clothing. Quite a fashion statement, don't you think?\n");
        printf("10 dpu  I wear the same clothes all the time, no matter the occasion,\n");
        printf("        forgetting to do laundry between wearings.\n");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("Enter your Dress code number here [0 to quit]: ");
	scanf("%d", &selection);
	clear_kb();
     } while (selection < 0 || selection > 10);

   if (selection == 0)
	   exit(1);
   else
	   return selection;
}

int get_height(void)
{
   int selection  = 99;

   do
     {
	clearscreen();
	printf("Shape (height)                                                     Page %i of %i\n", PAGES-(PAGES-3), PAGES);
	printf("===============================================================================\n");
        printf("1 s+++ I usually have to duck through doors.\n");
        printf("2 s++  I'm a basketball candidate.\n");
        printf("3 s+   I'm a little taller than most.\n");
        printf("4 s    I'm an average geek.\n");
        printf("5 s-   I look up to most people.\n");
        printf("6 s--  I look up to damn near everybody.\n");
        printf("7 s--- I take a phone book with me when I go out so I can eat dinner.\n");
        printf("\n");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("\n");
        printf("\n");
        printf("Enter your Shape (height) code number here [0 to quit]: ");
        scanf("%d", &selection);
	clear_kb();
     } while (selection < 0 || selection > 7);

   if (selection == 0)
	   exit(1);
   else
	   return selection;
}

int get_weight(void)
{
   int selection = 99;

   do
     {
	clearscreen();
	printf("Shape (weight)                                                     Page %i of %i\n", PAGES-(PAGES-4), PAGES);
	printf("===============================================================================\n");
        printf("1 s+++ I take up three movie seats.\n");
        printf("2 s++  I'm a linebacker candidate.\n");
        printf("3 s+   I'm a little rounder than most.\n");
        printf("4 s    I'm an average geek.\n");
        printf("5 s-   Everybody tells me to gain a few pounds.\n");
        printf("6 s--  I tend to have to fight against a strong breeze.\n");
        printf("7 s--- My bones are poking through my skin.\n");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("\n");
        printf("\n");
	printf("\n");
	printf("\n");
	printf("\n");
        printf("Enter your Shape (weight) code number here [0 to quit]: ");
        scanf("%d", &selection);
	clear_kb();
     } while (selection < 0 || selection > 7);

   if (selection == 0)
	   exit(1);
   else
	   return selection;
}

int get_age(void)
{
   int selection = 99;

   do
     {
	clearscreen();
	printf("Age                                                                Page %i of %i\n", PAGES-(PAGES-5), PAGES);
	printf("===============================================================================\n");
        printf(" 1 a+++   60 and up\n");
        printf(" 2 a++    50-59\n");
        printf(" 3 a+     40-49\n");
        printf(" 4 a      30-39\n");
        printf(" 5 a-     25-29\n");
        printf(" 6 a--    20-24\n");
        printf(" 7 a---   15-19\n");
        printf(" 8 a----  10-14\n");
        printf(" 9 a----- 9 and under (Geek in training?)\n");
        printf("10 a?     Immortal\n");
        printf("11 !a     It's none of your business how old I am\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("Enter your Age code number here [0 to quit]: ");
        scanf("%d", &selection);
	clear_kb();
     } while (selection < 0 || selection > 11);

   if (selection == 0)
	   exit(1);
   else
	   return selection;
}

