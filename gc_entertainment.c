/*
 gc_entertainment.c - Functions for the Entertainment section

 Geek Code Generator v1.7.3 - Generates your geek code
 Copyright (C) 1999-2003 Chris Gushue <chris@blackplasma.net>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <stdio.h>
#include "geekcode.h"

int get_startrek(void)
{
   int selection = 99;

   do
     {
	clearscreen();
	printf("Star Trek                                                         Page %i of %i\n", PAGES-(PAGES-23), PAGES);
	printf("===============================================================================\n");
        printf("1 t+++ It's not just a TV show, it's a religion. I know all about warp field\n");
        printf("       dynamics and the principles behind the transporter. I have memorized the\n");
        printf("       TECH manual. I speak Klingon. I go to cons with Vulcan ears on.\n");
        printf("       I have no life.\n");
        printf("2 t++  It's the best show around. I have all the episodes and the movies on\n");
        printf("       tape and can quote entire scenes verbatim. I've built a few of the model\n");
        printf("       kits too. But you'll never catch me at one of those conventions. Those\n");
        printf("       people are kooks.\n");
        printf("3 t+   It's a damn fine TV show and is one of the only things good on TV now.\n");
        printf("4 t    It's just another TV show.\n");
        printf("5 t-   Maybe it is just me, but I have no idea what the big deal with Star Trek\n");
        printf("       is. Perhaps I'm missing something but I just think it is bad drama.\n");
        printf("6 t--  Star Trek is just another Space Opera. William Shatner isn't an actor,\n");
        printf("       he's a poser! And what's with this Jean-Luc Picard? A Frenchman with a\n");
        printf("       British accent? Come on. Isn't Voyager just a rehash of Lost in Space?\n");
        printf("       Has Sisko even breathed in the last two seasons? Come on. I'd only watch\n");
        printf("       this show if my remote control broke.\n");
        printf("7 t--- Star Trek SUCKS! It is the worst crap I have ever seen! Hey, all you\n");
        printf("       Trekkies out there, GET A LIFE! (William Shatner is a t---)\n");
        printf("8 t*   I identify with Barclay, the greatest of the Trek Geeks.\n");
        printf("\n");
	printf("Enter your Star Trek code number here [0 to quit]: ");
	scanf("%d", &selection);
	clear_kb();
     } while (selection < 0 || selection > 8);

   if (selection == 0)
	   exit(1);
   else
	   return selection;
}

int get_babylon5(void)
{
   int selection = 99;

   do
     {
	clearscreen();
	printf("Babylon 5                                                         Page %i of %i\n", PAGES-(PAGES-24), PAGES);
	printf("===============================================================================\n");
        printf("1 5++++ I am J. Michael Straczynski\n");
        printf("2 5+++  I am a True Worshipper of the Church of Joe who lives eats breathes and\n");
        printf("        thinks Babylon 5, and has Evil thoughts about staling Joe's videotape\n");
        printf("        archives just to see episodes earlier. I am planning to break into the\n");
        printf("        bank and steal the triple-encoded synopsis of the 5-year arc.\n");
        printf("3 5++   Finally a show that shows what a real future would look like. None of\n");
        printf("        Picardian \"Let's talk about it and be friends\" crap. And what's this?\n");
        printf("        We finally get to see a bathroom! Over on that Enterprise, they've been\n");
        printf("        holding it for over seven years!\n");
        printf("4 5+    Babylon 5 certainly presents a fresh perspective in the Sci-Fi\n");
        printf("        universe. I watch it weekly.\n");
        printf("5 5     I've seen it, I am pretty indifferent to it.\n");
        printf("6 5-    This show is sub-par. The acting is wooden, the special effects are\n");
        printf("        obviously poor quality. In general, it seems like a very cheap\n");
        printf("        Star Trek ripoff.\n");
        printf("7 5--   You call this Sci-Fi? That is such a load of crap! This show is just a\n");
        printf("        a soap with bad actors, piss-poor effects, and lame storylines.\n");
        printf("        Puh-leese.\n");
        printf("\n");
        printf("\n");
        printf("\n");
	printf("Enter your Babylon 5 code number here [0 to quit]: ");
	scanf("%d", &selection);
	clear_kb();
     } while (selection < 0 || selection > 7);

   if (selection == 0)
	   exit(1);
   else
	   return selection;
}

int get_xfiles(void)
{
   int selection = 99;

   do
     {
	clearscreen();
	printf("X-Files                                                           Page %i of %i\n", PAGES-(PAGES-25), PAGES);
	printf("===============================================================================\n");
        printf("1 X++++ I am Chris Carter\n");
        printf("2 X+++  This is the BEST show on TV, and it's about time. I've seen everything\n");
        printf("        from David Duchovny and Gillian Anderson have ever done that has been\n");
        printf("        recorded and I'm a loyal Duchovny/Anderson fan. I've converted at least\n");
        printf("        10 people. I have every episode at SP, debate the fine details online,\n");
        printf("        and have a credit for at least 2 YAXAs.\n");
        printf("3 X++   This is one of the better shows I've seen. I wish I'd taped everything\n");
        printf("        from the start at SP, because I'm wearing out my EP tapes. I'll\n");
        printf("        periodically debate online. I've converted at least 5 people. I've\n");
        printf("        gotten a YAXA.\n");
        printf("4 X+    I've converted my family and watch the show when I remember. It's\n");
        printf("        really kinda fun.\n");
        printf("5 X     Ho hum. Just another FOX show.\n");
        printf("6 X-    It's ok if you like paranoia and conspiracy stories, but left's face\n");
        printf("        it, it's crap.\n");
        printf("7 X--   If I wanted to watch this kind of stuff, I'd talk to Oliver Stone.\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
	printf("Enter your X-Files code number here [0 to quit]: ");
	scanf("%d", &selection);
	clear_kb();
     } while (selection < 0 || selection > 7);

   if (selection == 0)
	   exit(1);
   else
	   return selection;
}

int get_rp(void)
{
   int selection = 99;

   do
     {
	clearscreen();
	printf("Role Playing                                                      Page %i of %i\n", PAGES-(PAGES-26), PAGES);
	printf("===============================================================================\n");
        printf("1 R+++ I've written and published my own gaming materials.\n");
        printf("2 R++  There is no life outside the roll of the die. I know all the piddly\n");
        printf("       rules of (chosen game). _MY_ own warped rules scare the rest of the\n");
        printf("       players.\n");
        printf("3 R+   I've got my weekly sessions set up and a character that I know better\n");
        printf("       than I know myself.\n");
        printf("4 R    Role-Playing? That's just something to do to kill a Saturday afternoon.\n");
        printf("5 R-   Gosh, what an utter waste of time!\n");
        printf("6 R--  Role-Players are instruments of pure evil.\n");
        printf("7 R--- I work for T$R.\n");
        printf("8 R*   I thought life WAS role-playing?\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
	printf("Enter your Role Playing code number here [0 to quit]: ");
	scanf("%d", &selection);
	clear_kb();
     } while (selection < 0 || selection > 8);

   if (selection == 0)
	   exit(1);
   else
	   return selection;
}

int get_television(void)
{
   int selection = 99;

   do
     {
	clearscreen();
	printf("Television                                                        Page %i of %i\n", PAGES-(PAGES-27), PAGES);
	printf("===============================================================================\n");
        printf("1 tv+++ There's nothing I can experience 'out there' that I can't see coming\n");
        printf("        over my satellite dish. I wish there were MORE channels. I live for the\n");
        printf("        O.J. Trial.\n");
        printf("2 tv++  I just leave the TV on, to make sure I don't miss anything.\n");
        printf("3 tv+   I watch some TV every day.\n");
        printf("4 tv    I watch only the shows that are actually worthwhile, such as those\n");
        printf("        found on PBS.\n");
        printf("5 tv-   I watch TV for the news and 'special programming'.\n");
        printf("6 tv--  I turn my TV on during natural disasters.\n");
        printf("7 !tv   I do not own a television.\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
	printf("Enter your Television code number here [0 to quit]: ");
	scanf("%d", &selection);
	clear_kb();
     } while (selection < 0 || selection > 7);

   if (selection == 0)
	   exit(1);
   else
	   return selection;
}

int get_books(void)
{
   int selection = 99;

   do
     {
	clearscreen();
	printf("Books                                                             Page %i of %i\n", PAGES-(PAGES-28), PAGES);
	printf("===============================================================================\n");
        printf("1 b++++ I read a book a day. I have library cards in three states. I have\n");
        printf("        discount cards from every major bookstore. I've ordered books from\n");
        printf("        another country to get my Favorite Author Fix.\n");
        printf("2 b+++  I consume a few books a week as part of a staple diet.\n");
        printf("3 b++   I find the time to get through at least one new book a month.\n");
        printf("4 b+    I enjoy reading, but don't get the time very often.\n");
        printf("5 b     I read the newspaper and the occasional book.\n");
        printf("6 b-    I read when there is no other way to get the information.\n");
        printf("7 b--   I did not actually READ the geek code, I just had someone tell me.\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
	printf("Enter your Books code number here [0 to quit]: ");
	scanf("%d", &selection);
	clear_kb();
     } while (selection < 0 || selection > 7);

   if (selection == 0)
	   exit(1);
   else
	   return selection;
}

int get_dilbert(void)
{
   int selection = 99;

   do
     {
	clearscreen();
	printf("Dilbert                                                           Page %i of %i\n", PAGES-(PAGES-29), PAGES);
	printf("===============================================================================\n");
        printf("1 DI+++++ I am Scott Adams\n");
        printf("2 DI++++  I've received mail from Scott Adams. i'm in the DNRC (Dogbert's New\n");
        printf("          Ruling Class)\n");
        printf("3 DI+++   I am a Dilbert prototype.\n");
        printf("4 DI++    I work with people that act a lot like Dilbert and his boss.\n");
        printf("5 DI+     I read Dilbert daily, often understanding it.\n");
        printf("6 DI      I read Dilbert infrequently, rarely understanding it.\n");
        printf("7 DI-     Is that the comic about the engineers?\n");
        printf("8 DI--    Don't read it, but I think the dog is kinda cute.\n");
        printf("9 DI---   I don't think it's funny to make fun of managers trying to best run\n");
        printf("          their organizational units.\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
	printf("Enter your Dilbert code number here [0 to quit]: ");
	scanf("%d", &selection);
	clear_kb();
     } while (selection < 0 || selection > 9);

   if (selection == 0)
	   exit(1);
   else
	   return selection;
}

int get_doom(void)
{
   int selection = 99;

   do
     {
	clearscreen();
	printf("Doom                                                              Page %i of %i\n", PAGES-(PAGES-30), PAGES);
	printf("===============================================================================\n");
        printf("1 D++++ I work for iD software.\n");
        printf("2 D+++  I crank out PWAD files daily, complete with new monsters, weaponry,\n");
        printf("        sounds, and maps. I'm a Doom god. I can solve the original maps in\n");
        printf("        nightmare mode with my eyes closed.\n");
        printf("3 D++   I've played the shareware version and bought the real one and I'm\n");
        printf("        actually pretty good at the game. I occasionally download PWAD files\n");
        printf("        and play them too.\n");
        printf("4 D+    It's a fun action game that is a nice diversion on a lazy afternoon.\n");
        printf("5 D     I've played the game and I'm pretty indifferent.\n");
        printf("6 D-    I've played the game and really didnt' think it was all that impressive\n");
        printf("7 D--   It's an overly-violent game and pure crap.\n");
        printf("8 D---  To hell with Doom, I miss Zork.\n");
        printf("9 D---- I've seen better on my Atari 2600.\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
	printf("Enter your Doom code number here [0 to quit]: ");
	scanf("%d", &selection);
	clear_kb();
     } while (selection < 0 || selection > 9);

   if (selection == 0)
	   exit(1);
   else
	   return selection;
}

int get_geekcode(void)
{
   int selection = 99;

   do
     {
	clearscreen();
	printf("The Geek Code                                                     Page %i of %i\n", PAGES-(PAGES-31), PAGES);
	printf("===============================================================================\n");
        printf("1 G+++++ I am Robert Hayden\n");
        printf("2 G++++  I have made a suggestion for future versions of the code (note that\n");
        printf("         making a suggestion just to get a G++++ rating doesn't count, you also\n");
        printf("         have to at least qualify for a G+++ rating)\n");
        printf("3 G+++   I have memorized the entire geek code, and can decode others' codes in\n");
        printf("         my head. I know where by heart to find the current version of the code\n");
        printf("         on the net.\n");
        printf("4 G++    I know what each letter means, but sometimes have to look up the\n");
        printf("         specifics.\n");
        printf("5 G+     I was once G++ (or higher), but the new versions are getting too long\n");
        printf("         and too complicated.\n");
        printf("6 G      I know what the Geek Code is and even did up this code.\n");
        printf("7 G-     What a tremendous waste of time the Geek code is.\n");
        printf("8 G--    Not only a waste of time, but it obviously shows that this Hayden guy\n");
        printf("         needs a life.\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
        printf("\n");
	printf("Enter your The Geek Code code number here [0 to quit]: ");
	scanf("%d", &selection);
	clear_kb();
     } while (selection < 0 || selection > 8);

   if (selection == 0)
	   exit(1);
   else
	   return selection;
}

