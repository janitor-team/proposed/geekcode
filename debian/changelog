geekcode (1.7.3-8) unstable; urgency=medium

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ Axel Beckert ]
  * Add patch to fix FTBFS with GCC 10 by fixing variable declarations in
    geekcode.{c,h}. (Closes: #957265)

 -- Eric Dorland <eric@debian.org>  Sun, 27 Sep 2020 22:55:13 -0400

geekcode (1.7.3-7) unstable; urgency=medium

  * Upgrade to debhelper 11
  * Move VCS to salsa.debian.org
  * Standards-Version to 4.1.4.1

 -- Eric Dorland <eric@debian.org>  Sun, 10 Jun 2018 16:23:15 -0400

geekcode (1.7.3-6) unstable; urgency=low

  * debian/control: Mark package Multi-Arch: foreign.
  * debian/geekcode.1: Reflow the manpage.
  * debian/control: Update Standards-Version to 3.9.5.
  * debian/patches/01-ldflags.diff: Add LDFLAGS to the Makefile to get the
    right dkpg-buildflags.
  * debian/control: Update to canonical Vcs-* fields.

 -- Eric Dorland <eric@debian.org>  Sun, 14 Sep 2014 19:47:41 -0400

geekcode (1.7.3-5) unstable; urgency=low

  * debian/control: Add Homepage, Vcs-Git and Vcs-Browser fields.
  * debian/patches/01-destdir.diff, debian/patches/series: Remove now
    unnecessary patch.

 -- Eric Dorland <eric@debian.org>  Sun, 19 Feb 2012 21:50:43 -0500

geekcode (1.7.3-4) unstable; urgency=low

  * debian/control, debian/rules, debian/compat: Use dh instead of cdbs
    and upgrade to dh level 9.
  * debian/source/format: Switch to 3.0 (quilt) source format.
  * patches/01-destdir.diff, patches/series: Add patch for DESTDIR in
    Makefile.
  * debian/control: Standards-Version to 3.9.2. Add ${misc:Depends}.
  * debian/geekcode.install, debian/rules: Install into /usr/games.

 -- Eric Dorland <eric@debian.org>  Sun, 19 Feb 2012 20:45:14 -0500

geekcode (1.7.3-3) unstable; urgency=low

  * debian/watch: Add watch file.
  * Bump Standards-Version to 3.7.2.2.

 -- Eric Dorland <eric@debian.org>  Sun, 29 Apr 2007 15:33:37 -0400

geekcode (1.7.3-2) unstable; urgency=low

  * debian/copyright: Apply patch from Jonas Genannt to fix up the
    copyright. (Closes: #343184)
  * debian/control: Upgrade Standards-Version while we're at it.

 -- Eric Dorland <eric@debian.org>  Wed, 14 Dec 2005 01:20:24 -0500

geekcode (1.7.3-1) unstable; urgency=low

  * New upstream release. (Closes: #250604)
  * Removed geekcode.txt from upstream tarball again since its license is
    non-free.
  * debian/copyright: Fix authors boilerplate and common licenses
    path. Thanks lintian.

 -- Eric Dorland <eric@debian.org>  Mon, 24 May 2004 22:11:08 -0400

geekcode (1.7.0.1-1) unstable; urgency=low

  * Not a new upstream release, removed geekcode.txt from upstream since
    it had a non-free license. (Closes: #227976)
  * debian/docs: Remove.
  * debian/geekcode.docs: Add, remove geekcode.txt from docs.
  * debian/control: S-V to 3.6.1.0.

 -- Eric Dorland <eric@debian.org>  Sun, 18 Jan 2004 23:51:35 -0500

geekcode (1.7-3) unstable; urgency=low

  * gc_computers.c: Changed Linux to GNU/Linux. (Closes: #189685)
  * debian/control:
    + Update Standards-Version to 3.5.10.0.
    + Build-Depend on cdbs and debhelper (>= 4.1).
    + Remove period from short description.
  * debian/compat: Update to 4.
  * debian/rules: Use the cdbs.
  * debian/dirs: Remove, obsolete.
  * debian/geekcode.manpages: Added, for the manpage.
  * debian/geekcode.install: Added, installs the binary.

 -- Eric Dorland <eric@debian.org>  Sun,  1 Jun 2003 20:02:23 -0400

geekcode (1.7-2) unstable; urgency=low

  * No reason why this should be priority extra.
  * Added debian/compat file.

 -- Eric Dorland <eric@debian.org>  Thu,  9 May 2002 06:50:15 -0400

geekcode (1.7-1) unstable; urgency=low

  * New upstream version. (Closes: #105765)
  * New maintainer, Eric Dorland <eric@debian.org>, with Andrea's blessing.
  * Removed useless README.Debian. (Closes: #100935)
  * Update Standards-Version to 3.5.6.0.
  * Updated to debhelper 3.
  * Added manpage from Jan Schaumann <jschauma@netmeister.org> from the
    Missing Man Pages Project, http://www.netmeister.org/misc/m2p2/.

 -- Eric Dorland <eric@debian.org>  Tue, 21 Aug 2001 01:11:56 -0400

geekcode (1.5-2) unstable; urgency=low

  * Close the #100935 bug.

 -- Andrea Fanfani <andrea@debian.org>  Sun, 15 Jul 2001 21:34:03 +0200

geekcode (1.5-1) unstable; urgency=low

  * Initial Release.

 -- Andrea Fanfani <andrea@debian.org>  Fri,  1 Oct 1999 19:39:29 +0200
