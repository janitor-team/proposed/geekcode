/*
 geekcode.c - Main program

 Geek Code Generator v1.7.3 - Generates your geek code
 Copyright (C) 1999-2003 Chris Gushue <chris@blackplasma.net>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include "geekcode.h"

int readgeek(char geekcode[64][16],char *data,int *j,int *k){
  int i;

  for(i=0;i<strlen(data);i++){
    if(data[i]==':'){geekcode[*j][*k]=0;(*j)++;geekcode[*j][0]=':';*k=1;} else
    if(data[i]==' ' || data[i]=='\n'){
      if(data[i+1]!=' ' && data[i+1]!='\n'){
        geekcode[*j][*k]=0;(*j)++;*k=0;
      }
    }else{
      geekcode[*j][*k]=data[i];
      (*k)++;
    }
  }
  return 0;
}

void read_code()
{
  char filename[256];
  char error[284];
  char data[1024];
  char geekcode[64][16];
  struct stat fileinfo;
  FILE *gcfile;
  int j=0,k=0;
  
  clearscreen();
  printf("Enter filename with Geekcode signature: ");
  if(fgets(filename,256,stdin) == NULL){
    perror("Error while reading the filename");
    exit(1);
  }
  filename[strlen(filename)-1]=0;
  if(stat(filename,&fileinfo) == -1){
    snprintf(error, sizeof error, "Error while checking file \"%s\"",filename);
    perror(error);
    exit(1);
  }
  if(S_ISREG(fileinfo.st_mode)!=1){
    fprintf(stderr,"Error, file \"%s\" isn't a regular file\n",filename);
    exit(1);
  }
  gcfile=fopen(filename,"r");
  if(gcfile==NULL){
    snprintf(error, sizeof error, "Error while opening file \"%s\"",filename);
    perror(error);
    exit(1);
  }
  if(fgets(data,1024,gcfile) == NULL){
    snprintf(error, sizeof error, "Error while reading file \"%s\"",filename);
    perror(error);
    exit(1);
  }
  if(strcmp(data,"-----BEGIN GEEK CODE BLOCK-----\n")!=0){
    fprintf(stderr,"Error, file \"%s\" isn't geekcode\n",filename);
    exit(1);
  }
  if(fgets(data,1024,gcfile) == NULL){  //read version
    fprintf(stderr,"Error, file \"%s\" isn't geekcode\n",filename);
    exit(1);
  }
  while(1){
    if(fgets(data,1024,gcfile) == NULL){
      fprintf(stderr,"Error, file \"%s\" isn't geekcode\n",filename);
      exit(1);
    }
    if(strcmp(data,"------END GEEK CODE BLOCK------\n") == 0) break;
    if(readgeek(geekcode,data,&j,&k) != 0){
      exit(1);
    }  
  }
  fclose(gcfile);
  translate(geekcode,j);
}

void create_code()
{

   /* Geek Type */
   gc_type          = get_type();

   /* Appearance */
   gc_dress         = get_dress();
   gc_height        = get_height();
   gc_weight        = get_weight();
   gc_age           = get_age();

   /* Computers */
   gc_computers     = get_computers();
   gc_unix_type     = get_unix_type();        /* Added in v1.1 */
   gc_unix          = get_unix(gc_unix_type); /* Modified in v1.2 */
   gc_perl          = get_perl();
   gc_linux         = get_linux();
   gc_emacs         = get_emacs();
   gc_www           = get_www();
   gc_usenet        = get_usenet();
   gc_oracle        = get_oracle();
   gc_kibo          = get_kibo();
   gc_windows       = get_windows();
   gc_os2           = get_os2();
   gc_mac           = get_mac();
   gc_vms           = get_vms();

   /* Politics */
   gc_social        = get_social();
   gc_economic      = get_economic();
   gc_cypher        = get_cypher();
   gc_pgp           = get_pgp();

   /* Entertainment */
   gc_startrek      = get_startrek();
   gc_babylon5      = get_babylon5();
   gc_xfiles        = get_xfiles();
   gc_rp            = get_rp();
   gc_television    = get_television();
   gc_books         = get_books();
   gc_dilbert       = get_dilbert();
   gc_doom          = get_doom();
   gc_geekcode      = get_geekcode();

   /* Lifestyle */
   gc_education     = get_education();
   gc_housing       = get_housing();
   gc_relationships = get_relationships();
   gc_sex_type      = get_sex_type();                /* Added in v1.2 */
   gc_sex           = get_sex(gc_sex_type);          /* Modified in v1.2 */

   show_geekcode(gc_sex_type);                       /* Modified in v1.2 */
   write_geekcode(gc_sex_type);			     /* Added in v1.7 */
}

int main()
{
   int selection = 99;

   clearscreen();
   printf("Geek Code Generator v%s - Generates your geek code\n", VERSION);  /* Modified in v1.2 */
   printf("Copyright (C) 1999-2003 Chris Gushue <chris@blackplasma.net>\n");
   printf("\n");
   do{
    printf("1) Create your own Geekcode\n");
    printf("2) Read a Geekcode\n");
    printf("\n");
    printf("Enter your choise here [0 to quit]: ");
    scanf("%d", &selection);
    clear_kb();
   }while(selection<0 || selection>2);
   switch(selection){
    case 1:
     create_code();
     break;
    case 2:
     read_code();
   }
   return 0;
}

void clear_kb(void)
{
   char junk[80];
   fgets(junk, 80, stdin); /* Changed to fgets() in v1.4 - is this a good way to use it? */
}

void show_geekcode(int x)
{
   char *sex_type;

   clearscreen();
   printf("Geek Code Generator v%s - Generates your geek code\n", VERSION);	/* Modified in v1.2 */
   printf("Copyright (C) 1999-2003 Chris Gushue <chris@blackplasma.net>\n");
   printf("\n");
   printf("This is free software; see the source for copying conditions.  There is NO\n");
   printf("warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE,\n");
   printf("to the extent permitted by law.\n");
   printf("\n");
   /* Geek code copyright added in v1.2 */
   printf("The Geek Code is copyright (C) 1993, 1994, 1995, 1996 by Robert A.\n");
   printf("Hayden. All rights reserved. You are free to distribute this code in\n");
   printf("electronic format provided that the file remains unmodified and this\n");
   printf("copyright notice remains attached. This copyright prohibits HTMLizing\n");
   printf("the code for publication on the web. If you wish to publish abstracts\n");
   printf("or portions of the code, contact the author for permission. If you\n");
   printf("wish to write an article about the Geek Code, please contact the\n");
   printf("author. All creatures not native to Earth are exempt from this\n");
   printf("copyright, however, they must prove that they qualify.\n");
   printf("\n");
   printf("-----BEGIN GEEK CODE BLOCK-----\n");
   printf("Version: 3.12\n");

   /* Line one start */
   switch (gc_type)
     {
      case  1: printf("GB ");  break;
      case  2: printf("GC ");  break;
      case  3: printf("GCA "); break;
      case  4: printf("GCM "); break;
      case  5: printf("GCS "); break;
      case  6: printf("GCC "); break;
      case  7: printf("GE ");  break;
      case  8: printf("GED "); break;
      case  9: printf("GFA "); break;
      case 10: printf("GG ");  break;
      case 11: printf("GH ");  break;
      case 12: printf("GIT "); break;
      case 13: printf("GJ ");  break;
      case 14: printf("GLS "); break;
      case 15: printf("GL ");  break;
      case 16: printf("GMC "); break;
      case 17: printf("GM ");  break;
      case 18: printf("GMD "); break;
      case 19: printf("GMU "); break;
      case 20: printf("GPA "); break;
      case 21: printf("GP ");  break;
      case 22: printf("GS ");  break;
      case 23: printf("GSS "); break;
      case 24: printf("GTW "); break;
      case 25: printf("GO ");  break;
      case 26: printf("GU ");  break;
      case 27: printf("G! ");  break;
      case 28: printf("GAT "); break;
     }

   switch (gc_dress)
     {
      case  1: printf("d++ ");  break;
      case  2: printf("d+ ");   break;
      case  3: printf("d ");    break;
      case  4: printf("d- ");   break;
      case  5: printf("d-- ");  break;
      case  6: printf("d--- "); break;
      case  7: printf("dx ");   break;
      case  8: printf("d? ");   break;
      case  9: printf("!d ");   break;
      case 10: printf("dpu ");  break;
     }

   switch (gc_height)
     {
      case 1: printf("s+++:"); break;
      case 2: printf("s++:");  break;
      case 3: printf("s+:");   break;
      case 4: printf("s:");    break;
      case 5: printf("s-:");   break;
      case 6: printf("s--:");  break;
      case 7: printf("s---:"); break;
     }

   switch (gc_weight)
     {
      case 1: printf("+++ "); break;
      case 2: printf("++ ");  break;
      case 3: printf("+ ");   break;
      case 4: printf(" ");    break;
      case 5: printf("- ");   break;
      case 6: printf("-- ");  break;
      case 7: printf("--- "); break;
     }

   switch (gc_age)
     {
      case  1: printf("a+++ ");   break;
      case  2: printf("a++ ");    break;
      case  3: printf("a+ ");     break;
      case  4: printf("a ");      break;
      case  5: printf("a- ");     break;
      case  6: printf("a-- ");    break;
      case  7: printf("a--- ");   break;
      case  8: printf("a---- ");  break;
      case  9: printf("a----- "); break;
      case 10: printf("a? ");     break;
      case 11: printf("!a ");     break;
     }

   switch (gc_computers)
     {
      case 1: printf("C++++ "); break;
      case 2: printf("C+++ ");  break;
      case 3: printf("C++ ");   break;
      case 4: printf("C+ ");    break;
      case 5: printf("C ");     break;
      case 6: printf("C- ");    break;
      case 7: printf("C-- ");   break;
      case 8: printf("C--- ");  break;
     }

   /* Added in v1.1 */
   switch (gc_unix_type)
     {
      case  1: printf("UB"); break;
      case  2: printf("UL"); break;
      case  3: printf("UU"); break;
      case  4: printf("UA"); break;
      case  5: printf("UV"); break;
      case  6: printf("UH"); break;
      case  7: printf("UI"); break;
      case  8: printf("UO"); break;
      case  9: printf("US"); break;
      case 10: printf("UC"); break;
      case 11: printf("UX"); break;
      case 12: printf("U*"); break;
     }

   switch (gc_unix)
     {
      case 1: printf("++++ "); break;
      case 2: printf("+++ ");  break;
      case 3: printf("++ ");   break;
      case 4: printf("+ ");    break;
      case 5: printf(" ");     break;
      case 6: printf("- ");    break;
      case 7: printf("-- ");   break;
      case 8: printf("--- ");  break;
     }

   switch (gc_perl)
     {
      case  1: printf("P+++++ "); break;
      case  2: printf("P++++ ");  break;
      case  3: printf("P+++ ");   break;
      case  4: printf("P++ ");    break;
      case  5: printf("P+ ");     break;
      case  6: printf("P ");      break;
      case  7: printf("P- ");     break;
      case  8: printf("P-- ");    break;
      case  9: printf("P--- ");   break;
      case 10: printf("P! ");     break;
     }

   switch (gc_linux)
     {
      case 1: printf("L+++++ "); break;
      case 2: printf("L++++ ");  break;
      case 3: printf("L+++ ");   break;
      case 4: printf("L++ ");    break;
      case 5: printf("L+ ");     break;
      case 6: printf("L ");      break;
      case 7: printf("L- ");     break;
      case 8: printf("L-- ");    break;
      case 9: printf("L--- ");   break;
     }

   switch (gc_emacs)
     {
      case 1: printf("E+++ ");  break;
      case 2: printf("E++ ");   break;
      case 3: printf("E+ ");    break;
      case 4: printf("E ");     break;
      case 5: printf("E- ");    break;
      case 6: printf("E-- ");   break;
      case 7: printf("E--- ");  break;
      case 8: printf("E---- "); break;
     }

   switch (gc_www)
     {
      case 1: printf("W+++ "); break;
      case 2: printf("W++ ");  break;
      case 3: printf("W+ ");   break;
      case 4: printf("W ");    break;
      case 5: printf("W- ");   break;
      case 6: printf("W-- ");  break;
     }

   switch (gc_usenet)
     {
      case  1: printf("N++++ "); break;
      case  2: printf("N+++ ");  break;
      case  3: printf("N++ ");   break;
      case  4: printf("N+ ");    break;
      case  5: printf("N ");     break;
      case  6: printf("N- ");    break;
      case  7: printf("N-- ");   break;
      case  8: printf("N--- ");  break;
      case  9: printf("N---- "); break;
      case 10: printf("N* ");    break;
     }

   switch (gc_oracle)
     {
      case 1: printf("o+++++ "); break;
      case 2: printf("o++++ ");  break;
      case 3: printf("o+++ ");   break;
      case 4: printf("o++ ");    break;
      case 5: printf("o+ ");     break;
      case 6: printf("o ");      break;
      case 7: printf("o- ");     break;
      case 8: printf("o-- ");    break;
     }

   switch (gc_kibo)
     {
      case  1: printf("K++++++ "); break;
      case  2: printf("K+++++ ");  break;
      case  3: printf("K++++ ");   break;
      case  4: printf("K+++ ");    break;
      case  5: printf("K++ ");     break;
      case  6: printf("K+ ");      break;
      case  7: printf("K ");       break;
      case  8: printf("K- ");      break;
      case  9: printf("K-- ");     break;
      case 10: printf("K--- ");    break;
      case 11: printf("K---- ");   break;
     }

   switch (gc_windows)
     {
      case 1: printf("w+++++ "); break;
      case 2: printf("w++++ ");  break;
      case 3: printf("w+++ ");   break;
      case 4: printf("w++ ");    break;
      case 5: printf("w+ ");     break;
      case 6: printf("w ");      break;
      case 7: printf("w- ");     break;
      case 8: printf("w-- ");    break;
      case 9: printf("w--- ");   break;
     }

   printf("\n");
   /* Line one end */

   switch (gc_os2)
     {
      case 1: printf("O+++ ");  break;
      case 2: printf("O++ ");   break;
      case 3: printf("O+ ");    break;
      case 4: printf("O ");     break;
      case 5: printf("O- ");    break;
      case 6: printf("O-- ");   break;
      case 7: printf("O--- ");  break;
      case 8: printf("O---- "); break;
     }

   switch (gc_mac)
     {
      case 1: printf("M++ "); break;
      case 2: printf("M+ ");  break;
      case 3: printf("M ");   break;
      case 4: printf("M- ");  break;
      case 5: printf("M-- "); break;
     }

   switch (gc_vms)
     {
      case 1: printf("V+++ "); break;
      case 2: printf("V++ ");  break;
      case 3: printf("V+ ");   break;
      case 4: printf("V ");    break;
      case 5: printf("V- ");   break;
      case 6: printf("V-- ");  break;
     }

   switch (gc_social)
     {
      case 1: printf("PS+++ "); break;
      case 2: printf("PS++ ");  break;
      case 3: printf("PS+ ");   break;
      case 4: printf("PS ");    break;
      case 5: printf("PS- ");   break;
      case 6: printf("PS-- ");  break;
      case 7: printf("PS--- "); break;
     }

   switch (gc_economic)
     {
      case 1: printf("PE+++ "); break;
      case 2: printf("PE++ ");  break;
      case 3: printf("PE+ ");   break;
      case 4: printf("PE ");    break;
      case 5: printf("PE- ");   break;
      case 6: printf("PE-- ");  break;
     }

   switch (gc_cypher)
     {
      case 1: printf("Y+++ "); break;
      case 2: printf("Y++ ");  break;
      case 3: printf("Y+ ");   break;
      case 4: printf("Y ");    break;
      case 5: printf("Y- ");   break;
      case 6: printf("Y-- ");  break;
      case 7: printf("Y--- "); break;
     }

   switch (gc_pgp)
     {
      case 1: printf("PGP++++ "); break;
      case 2: printf("PGP+++ ");  break;
      case 3: printf("PGP++ ");   break;
      case 4: printf("PGP+ ");    break;
      case 5: printf("PGP ");     break;
      case 6: printf("PGP- ");    break;
      case 7: printf("PGP-- ");   break;
      case 8: printf("PGP--- ");  break;
      case 9: printf("PGP---- "); break;
     }

   switch (gc_startrek)
     {
      case 1: printf("t+++ "); break;
      case 2: printf("t++ ");  break;
      case 3: printf("t+ ");   break;
      case 4: printf("t ");    break;
      case 5: printf("t- ");   break;
      case 6: printf("t-- ");  break;
      case 7: printf("t--- "); break;
     }

   switch (gc_babylon5)
     {
      case 1: printf("5++++ "); break;
      case 2: printf("5+++ ");  break;
      case 3: printf("5++ ");   break;
      case 4: printf("5+ ");    break;
      case 5: printf("5 ");     break;
      case 6: printf("5- ");    break;
      case 7: printf("5-- ");   break;
     }

   switch (gc_xfiles)
     {
      case 1: printf("X++++ "); break;
      case 2: printf("X+++ ");  break;
      case 3: printf("X++ ");   break;
      case 4: printf("X+ ");    break;
      case 5: printf("X ");     break;
      case 6: printf("X- ");    break;
      case 7: printf("X-- ");   break;
     }

   switch (gc_rp)
     {
      case 1: printf("R+++ "); break;
      case 2: printf("R++ ");  break;
      case 3: printf("R+ ");   break;
      case 4: printf("R ");    break;
      case 5: printf("R- ");   break;
      case 6: printf("R-- ");  break;
      case 7: printf("R--- "); break;
      case 8: printf("R* ");   break;
     }

   switch (gc_television)
     {
      case 1: printf("tv+++ "); break;
      case 2: printf("tv++ ");  break;
      case 3: printf("tv+ ");   break;
      case 4: printf("tv ");    break;
      case 5: printf("tv- ");   break;
      case 6: printf("tv-- ");  break;
      case 7: printf("!tv ");   break;
     }

   switch (gc_books)
     {
      case 1: printf("b++++ "); break;
      case 2: printf("b+++ ");  break;
      case 3: printf("b++ ");   break;
      case 4: printf("b+ ");    break;
      case 5: printf("b ");     break;
      case 6: printf("b- ");    break;
      case 7: printf("b-- ");   break;
     }

   switch (gc_dilbert)
     {
      case 1: printf("DI+++++ "); break;
      case 2: printf("DI++++ ");  break;
      case 3: printf("DI+++ ");   break;
      case 4: printf("DI++ ");    break;
      case 5: printf("DI+ ");     break;
      case 6: printf("DI ");      break;
      case 7: printf("DI- ");     break;
      case 8: printf("DI-- ");    break;
      case 9: printf("DI--- ");   break;
     }

   switch (gc_doom)
     {
      case 1: printf("D++++ "); break;
      case 2: printf("D+++ ");  break;
      case 3: printf("D++ ");   break;
      case 4: printf("D+ ");    break;
      case 5: printf("D ");     break;
      case 6: printf("D- ");    break;
      case 7: printf("D-- ");   break;
      case 8: printf("D--- ");  break;
      case 9: printf("D---- "); break;
     }

   printf("\n");
   /* Line two end */

   switch (gc_geekcode)
     {
      case 1: printf("G+++++ "); break;
      case 2: printf("G++++ ");  break;
      case 3: printf("G+++ ");   break;
      case 4: printf("G++ ");    break;
      case 5: printf("G+ ");     break;
      case 6: printf("G ");      break;
      case 7: printf("G- ");     break;
      case 8: printf("G-- ");    break;
     }

   switch (gc_education)
     {
      case 1: printf("e+++++ "); break;
      case 2: printf("e++++ ");  break;
      case 3: printf("e+++ ");   break;
      case 4: printf("e++ ");    break;
      case 5: printf("e+ ");     break;
      case 6: printf("e ");      break;
      case 7: printf("e- ");     break;
      case 8: printf("e-- ");    break;
      case 9: printf("e* ");     break;
     }

   switch (gc_housing)
     {
      case 1: printf("h++ ");   break;
      case 2: printf("h+ ");    break;
      case 3: printf("h ");     break;
      case 4: printf("h- ");    break;
      case 5: printf("h-- ");   break;
      case 6: printf("h--- ");  break;
      case 7: printf("h---- "); break;
      case 8: printf("h! ");    break;
      case 9: printf("h* ");    break;
     }

   switch (gc_relationships)
     {
      case  1: printf("r+++ "); break;
      case  2: printf("r++ ");  break;
      case  3: printf("r+ ");   break;
      case  4: printf("r ");    break;
      case  5: printf("r- ");   break;
      case  6: printf("r-- ");  break;
      case  7: printf("r--- "); break;
      case  8: printf("!r ");   break;
      case  9: printf("r* ");   break;
      case 10: printf("r%% ");  break;
     }

   /* Added in v1.2 */
   switch (x)
     {
      case  1: sex_type="x"; break;
      case  2: sex_type="y"; break;
      case  3: sex_type="z"; break;
      default: sex_type="z"; break;
     }

   /* Modified in v1.2 */
   switch (gc_sex)
     {
      case  1: printf("%s+++++ ", sex_type); break;
      case  2: printf("%s++++ ",  sex_type); break;
      case  3: printf("%s+++ ",   sex_type); break;
      case  4: printf("%s++ ",    sex_type); break;
      case  5: printf("%s+ ",     sex_type); break;
      case  6: printf("%s ",      sex_type); break;
      case  7: printf("%s- ",     sex_type); break;
      case  8: printf("%s-- ",    sex_type); break;
      case  9: printf("%s--- ",   sex_type); break;
      case 10: printf("%s* ",     sex_type); break;
      case 11: printf("%s** ",    sex_type); break;
      case 12: printf("!%s ",     sex_type); break;
      case 13: printf("%s? ",     sex_type); break;
      case 14: printf("!%s+ ",    sex_type); break;
     }

    printf("\n");
    printf("------END GEEK CODE BLOCK------\n");
}

/* 
 I haven't fully tested the following code, but it looks ok.
 Submitted by Ingo Hoffmann <lasombra@infolink.com.br>
*/ /* Added in v1.7 */

void write_geekcode(int x)
{
   char *sex_type;
   FILE *fcode;

   fcode = fopen("geekcode.sig", "w");

   fprintf(fcode, "-----BEGIN GEEK CODE BLOCK-----\n");
   fprintf(fcode, "Version: 3.12\n");

   /* Line one start */
   switch (gc_type)
     {
      case  1: fprintf(fcode, "GB ");  break;
      case  2: fprintf(fcode, "GC ");  break;
      case  3: fprintf(fcode, "GCA "); break;
      case  4: fprintf(fcode, "GCM "); break;
      case  5: fprintf(fcode, "GCS "); break;
      case  6: fprintf(fcode, "GCC "); break;
      case  7: fprintf(fcode, "GE ");  break;
      case  8: fprintf(fcode, "GED "); break;
      case  9: fprintf(fcode, "GFA "); break;
      case 10: fprintf(fcode, "GG ");  break;
      case 11: fprintf(fcode, "GH ");  break;
      case 12: fprintf(fcode, "GIT "); break;
      case 13: fprintf(fcode, "GJ ");  break;
      case 14: fprintf(fcode, "GLS "); break;
      case 15: fprintf(fcode, "GL ");  break;
      case 16: fprintf(fcode, "GMC "); break;
      case 17: fprintf(fcode, "GM ");  break;
      case 18: fprintf(fcode, "GMD "); break;
      case 19: fprintf(fcode, "GMU "); break;
      case 20: fprintf(fcode, "GPA "); break;
      case 21: fprintf(fcode, "GP ");  break;
      case 22: fprintf(fcode, "GS ");  break;
      case 23: fprintf(fcode, "GSS "); break;
      case 24: fprintf(fcode, "GTW "); break;
      case 25: fprintf(fcode, "GO ");  break;
      case 26: fprintf(fcode, "GU ");  break;
      case 27: fprintf(fcode, "G! ");  break;
      case 28: fprintf(fcode, "GAT "); break;
     }

   switch (gc_dress)
     {
      case  1: fprintf(fcode, "d++ ");  break;
      case  2: fprintf(fcode, "d+ ");   break;
      case  3: fprintf(fcode, "d ");    break;
      case  4: fprintf(fcode, "d- ");   break;
      case  5: fprintf(fcode, "d-- ");  break;
      case  6: fprintf(fcode, "d--- "); break;
      case  7: fprintf(fcode, "dx ");   break;
      case  8: fprintf(fcode, "d? ");   break;
      case  9: fprintf(fcode, "!d ");   break;
      case 10: fprintf(fcode, "dpu ");  break;
     }

   switch (gc_height)
     {
      case 1: fprintf(fcode, "s+++:"); break;
      case 2: fprintf(fcode, "s++:");  break;
      case 3: fprintf(fcode, "s+:");   break;
      case 4: fprintf(fcode, "s:");    break;
      case 5: fprintf(fcode, "s-:");   break;
      case 6: fprintf(fcode, "s--:");  break;
      case 7: fprintf(fcode, "s---:"); break;
     }

   switch (gc_weight)
     {
      case 1: fprintf(fcode, "+++ "); break;
      case 2: fprintf(fcode, "++ ");  break;
      case 3: fprintf(fcode, "+ ");   break;
      case 4: fprintf(fcode, " ");    break;
      case 5: fprintf(fcode, "- ");   break;
      case 6: fprintf(fcode, "-- ");  break;
      case 7: fprintf(fcode, "--- "); break;
     }

   switch (gc_age)
     {
      case  1: fprintf(fcode, "a+++ ");   break;
      case  2: fprintf(fcode, "a++ ");    break;
      case  3: fprintf(fcode, "a+ ");     break;
      case  4: fprintf(fcode, "a ");      break;
      case  5: fprintf(fcode, "a- ");     break;
      case  6: fprintf(fcode, "a-- ");    break;
      case  7: fprintf(fcode, "a--- ");   break;
      case  8: fprintf(fcode, "a---- ");  break;
      case  9: fprintf(fcode, "a----- "); break;
      case 10: fprintf(fcode, "a? ");     break;
      case 11: fprintf(fcode, "!a ");     break;
     }

   switch (gc_computers)
     {
      case 1: fprintf(fcode, "C++++ "); break;
      case 2: fprintf(fcode, "C+++ ");  break;
      case 3: fprintf(fcode, "C++ ");   break;
      case 4: fprintf(fcode, "C+ ");    break;
      case 5: fprintf(fcode, "C ");     break;
      case 6: fprintf(fcode, "C- ");    break;
      case 7: fprintf(fcode, "C-- ");   break;
      case 8: fprintf(fcode, "C--- ");  break;
     }

   /* Added in v1.1 */
   switch (gc_unix_type)
     {
      case  1: fprintf(fcode, "UB"); break;
      case  2: fprintf(fcode, "UL"); break;
      case  3: fprintf(fcode, "UU"); break;
      case  4: fprintf(fcode, "UA"); break;
      case  5: fprintf(fcode, "UV"); break;
      case  6: fprintf(fcode, "UH"); break;
      case  7: fprintf(fcode, "UI"); break;
      case  8: fprintf(fcode, "UO"); break;
      case  9: fprintf(fcode, "US"); break;
      case 10: fprintf(fcode, "UC"); break;
      case 11: fprintf(fcode, "UX"); break;
      case 12: fprintf(fcode, "U*"); break;
     }

   switch (gc_unix)
     {
      case 1: fprintf(fcode, "++++ "); break;
      case 2: fprintf(fcode, "+++ ");  break;
      case 3: fprintf(fcode, "++ ");   break;
      case 4: fprintf(fcode, "+ ");    break;
      case 5: fprintf(fcode, " ");     break;
      case 6: fprintf(fcode, "- ");    break;
      case 7: fprintf(fcode, "-- ");   break;
      case 8: fprintf(fcode, "--- ");  break;
     }

   switch (gc_perl)
     {
      case  1: fprintf(fcode, "P+++++ "); break;
      case  2: fprintf(fcode, "P++++ ");  break;
      case  3: fprintf(fcode, "P+++ ");   break;
      case  4: fprintf(fcode, "P++ ");    break;
      case  5: fprintf(fcode, "P+ ");     break;
      case  6: fprintf(fcode, "P ");      break;
      case  7: fprintf(fcode, "P- ");     break;
      case  8: fprintf(fcode, "P-- ");    break;
      case  9: fprintf(fcode, "P--- ");   break;
      case 10: fprintf(fcode, "P! ");     break;
     }

   switch (gc_linux)
     {
      case 1: fprintf(fcode, "L+++++ "); break;
      case 2: fprintf(fcode, "L++++ ");  break;
      case 3: fprintf(fcode, "L+++ ");   break;
      case 4: fprintf(fcode, "L++ ");    break;
      case 5: fprintf(fcode, "L+ ");     break;
      case 6: fprintf(fcode, "L ");      break;
      case 7: fprintf(fcode, "L- ");     break;
      case 8: fprintf(fcode, "L-- ");    break;
      case 9: fprintf(fcode, "L--- ");   break;
     }

   switch (gc_emacs)
     {
      case 1: fprintf(fcode, "E+++ ");  break;
      case 2: fprintf(fcode, "E++ ");   break;
      case 3: fprintf(fcode, "E+ ");    break;
      case 4: fprintf(fcode, "E ");     break;
      case 5: fprintf(fcode, "E- ");    break;
      case 6: fprintf(fcode, "E-- ");   break;
      case 7: fprintf(fcode, "E--- ");  break;
      case 8: fprintf(fcode, "E---- "); break;
     }

   switch (gc_www)
     {
      case 1: fprintf(fcode, "W+++ "); break;
      case 2: fprintf(fcode, "W++ ");  break;
      case 3: fprintf(fcode, "W+ ");   break;
      case 4: fprintf(fcode, "W ");    break;
      case 5: fprintf(fcode, "W- ");   break;
      case 6: fprintf(fcode, "W-- ");  break;
     }

   switch (gc_usenet)
     {
      case  1: fprintf(fcode, "N++++ "); break;
      case  2: fprintf(fcode, "N+++ ");  break;
      case  3: fprintf(fcode, "N++ ");   break;
      case  4: fprintf(fcode, "N+ ");    break;
      case  5: fprintf(fcode, "N ");     break;
      case  6: fprintf(fcode, "N- ");    break;
      case  7: fprintf(fcode, "N-- ");   break;
      case  8: fprintf(fcode, "N--- ");  break;
      case  9: fprintf(fcode, "N---- "); break;
      case 10: fprintf(fcode, "N* ");    break;
     }

   switch (gc_oracle)
     {
      case 1: fprintf(fcode, "o+++++ "); break;
      case 2: fprintf(fcode, "o++++ ");  break;
      case 3: fprintf(fcode, "o+++ ");   break;
      case 4: fprintf(fcode, "o++ ");    break;
      case 5: fprintf(fcode, "o+ ");     break;
      case 6: fprintf(fcode, "o ");      break;
      case 7: fprintf(fcode, "o- ");     break;
      case 8: fprintf(fcode, "o-- ");    break;
     }

   switch (gc_kibo)
     {
      case  1: fprintf(fcode, "K++++++ "); break;
      case  2: fprintf(fcode, "K+++++ ");  break;
      case  3: fprintf(fcode, "K++++ ");   break;
      case  4: fprintf(fcode, "K+++ ");    break;
      case  5: fprintf(fcode, "K++ ");     break;
      case  6: fprintf(fcode, "K+ ");      break;
      case  7: fprintf(fcode, "K ");       break;
      case  8: fprintf(fcode, "K- ");      break;
      case  9: fprintf(fcode, "K-- ");     break;
      case 10: fprintf(fcode, "K--- ");    break;
      case 11: fprintf(fcode, "K---- ");   break;
     }

   switch (gc_windows)
     {
      case 1: fprintf(fcode, "w+++++ "); break;
      case 2: fprintf(fcode, "w++++ ");  break;
      case 3: fprintf(fcode, "w+++ ");   break;
      case 4: fprintf(fcode, "w++ ");    break;
      case 5: fprintf(fcode, "w+ ");     break;
      case 6: fprintf(fcode, "w ");      break;
      case 7: fprintf(fcode, "w- ");     break;
      case 8: fprintf(fcode, "w-- ");    break;
      case 9: fprintf(fcode, "w--- ");   break;
     }

   fprintf(fcode, "\n");
   /* Line one end */

   switch (gc_os2)
     {
      case 1: fprintf(fcode, "O+++ ");  break;
      case 2: fprintf(fcode, "O++ ");   break;
      case 3: fprintf(fcode, "O+ ");    break;
      case 4: fprintf(fcode, "O ");     break;
      case 5: fprintf(fcode, "O- ");    break;
      case 6: fprintf(fcode, "O-- ");   break;
      case 7: fprintf(fcode, "O--- ");  break;
      case 8: fprintf(fcode, "O---- "); break;
     }

   switch (gc_mac)
     {
      case 1: fprintf(fcode, "M++ "); break;
      case 2: fprintf(fcode, "M+ ");  break;
      case 3: fprintf(fcode, "M ");   break;
      case 4: fprintf(fcode, "M- ");  break;
      case 5: fprintf(fcode, "M-- "); break;
     }

   switch (gc_vms)
     {
      case 1: fprintf(fcode, "V+++ "); break;
      case 2: fprintf(fcode, "V++ ");  break;
      case 3: fprintf(fcode, "V+ ");   break;
      case 4: fprintf(fcode, "V ");    break;
      case 5: fprintf(fcode, "V- ");   break;
      case 6: fprintf(fcode, "V-- ");  break;
     }

   switch (gc_social)
     {
      case 1: fprintf(fcode, "PS+++ "); break;
      case 2: fprintf(fcode, "PS++ ");  break;
      case 3: fprintf(fcode, "PS+ ");   break;
      case 4: fprintf(fcode, "PS ");    break;
      case 5: fprintf(fcode, "PS- ");   break;
      case 6: fprintf(fcode, "PS-- ");  break;
      case 7: fprintf(fcode, "PS--- "); break;
     }

   switch (gc_economic)
     {
      case 1: fprintf(fcode, "PE+++ "); break;
      case 2: fprintf(fcode, "PE++ ");  break;
      case 3: fprintf(fcode, "PE+ ");   break;
      case 4: fprintf(fcode, "PE ");    break;
      case 5: fprintf(fcode, "PE- ");   break;
      case 6: fprintf(fcode, "PE-- ");  break;
     }

   switch (gc_cypher)
     {
      case 1: fprintf(fcode, "Y+++ "); break;
      case 2: fprintf(fcode, "Y++ ");  break;
      case 3: fprintf(fcode, "Y+ ");   break;
      case 4: fprintf(fcode, "Y ");    break;
      case 5: fprintf(fcode, "Y- ");   break;
      case 6: fprintf(fcode, "Y-- ");  break;
      case 7: fprintf(fcode, "Y--- "); break;
     }

   switch (gc_pgp)
     {
      case 1: fprintf(fcode, "PGP++++ "); break;
      case 2: fprintf(fcode, "PGP+++ ");  break;
      case 3: fprintf(fcode, "PGP++ ");   break;
      case 4: fprintf(fcode, "PGP+ ");    break;
      case 5: fprintf(fcode, "PGP ");     break;
      case 6: fprintf(fcode, "PGP- ");    break;
      case 7: fprintf(fcode, "PGP-- ");   break;
      case 8: fprintf(fcode, "PGP--- ");  break;
      case 9: fprintf(fcode, "PGP---- "); break;
     }

   switch (gc_startrek)
     {
      case 1: fprintf(fcode, "t+++ "); break;
      case 2: fprintf(fcode, "t++ ");  break;
      case 3: fprintf(fcode, "t+ ");   break;
      case 4: fprintf(fcode, "t ");    break;
      case 5: fprintf(fcode, "t- ");   break;
      case 6: fprintf(fcode, "t-- ");  break;
      case 7: fprintf(fcode, "t--- "); break;
     }

   switch (gc_babylon5)
     {
      case 1: fprintf(fcode, "5++++ "); break;
      case 2: fprintf(fcode, "5+++ ");  break;
      case 3: fprintf(fcode, "5++ ");   break;
      case 4: fprintf(fcode, "5+ ");    break;
      case 5: fprintf(fcode, "5 ");     break;
      case 6: fprintf(fcode, "5- ");    break;
      case 7: fprintf(fcode, "5-- ");   break;
     }

   switch (gc_xfiles)
     {
      case 1: fprintf(fcode, "X++++ "); break;
      case 2: fprintf(fcode, "X+++ ");  break;
      case 3: fprintf(fcode, "X++ ");   break;
      case 4: fprintf(fcode, "X+ ");    break;
      case 5: fprintf(fcode, "X ");     break;
      case 6: fprintf(fcode, "X- ");    break;
      case 7: fprintf(fcode, "X-- ");   break;
     }

   switch (gc_rp)
     {
      case 1: fprintf(fcode, "R+++ "); break;
      case 2: fprintf(fcode, "R++ ");  break;
      case 3: fprintf(fcode, "R+ ");   break;
      case 4: fprintf(fcode, "R ");    break;
      case 5: fprintf(fcode, "R- ");   break;
      case 6: fprintf(fcode, "R-- ");  break;
      case 7: fprintf(fcode, "R--- "); break;
      case 8: fprintf(fcode, "R* ");   break;
     }

   switch (gc_television)
     {
      case 1: fprintf(fcode, "tv+++ "); break;
      case 2: fprintf(fcode, "tv++ ");  break;
      case 3: fprintf(fcode, "tv+ ");   break;
      case 4: fprintf(fcode, "tv ");    break;
      case 5: fprintf(fcode, "tv- ");   break;
      case 6: fprintf(fcode, "tv-- ");  break;
      case 7: fprintf(fcode, "!tv ");   break;
     }

   switch (gc_books)
     {
      case 1: fprintf(fcode, "b++++ "); break;
      case 2: fprintf(fcode, "b+++ ");  break;
      case 3: fprintf(fcode, "b++ ");   break;
      case 4: fprintf(fcode, "b+ ");    break;
      case 5: fprintf(fcode, "b ");     break;
      case 6: fprintf(fcode, "b- ");    break;
      case 7: fprintf(fcode, "b-- ");   break;
     }

   switch (gc_dilbert)
     {
      case 1: fprintf(fcode, "DI+++++ "); break;
      case 2: fprintf(fcode, "DI++++ ");  break;
      case 3: fprintf(fcode, "DI+++ ");   break;
      case 4: fprintf(fcode, "DI++ ");    break;
      case 5: fprintf(fcode, "DI+ ");     break;
      case 6: fprintf(fcode, "DI ");      break;
      case 7: fprintf(fcode, "DI- ");     break;
      case 8: fprintf(fcode, "DI-- ");    break;
      case 9: fprintf(fcode, "DI--- ");   break;
     }

   switch (gc_doom)
     {
      case 1: fprintf(fcode, "D++++ "); break;
      case 2: fprintf(fcode, "D+++ ");  break;
      case 3: fprintf(fcode, "D++ ");   break;
      case 4: fprintf(fcode, "D+ ");    break;
      case 5: fprintf(fcode, "D ");     break;
      case 6: fprintf(fcode, "D- ");    break;
      case 7: fprintf(fcode, "D-- ");   break;
      case 8: fprintf(fcode, "D--- ");  break;
      case 9: fprintf(fcode, "D---- "); break;
     }

   fprintf(fcode, "\n");
   /* Line two end */

   switch (gc_geekcode)
     {
      case 1: fprintf(fcode, "G+++++ "); break;
      case 2: fprintf(fcode, "G++++ ");  break;
      case 3: fprintf(fcode, "G+++ ");   break;
      case 4: fprintf(fcode, "G++ ");    break;
      case 5: fprintf(fcode, "G+ ");     break;
      case 6: fprintf(fcode, "G ");      break;
      case 7: fprintf(fcode, "G- ");     break;
      case 8: fprintf(fcode, "G-- ");    break;
     }

   switch (gc_education)
     {
      case 1: fprintf(fcode, "e+++++ "); break;
      case 2: fprintf(fcode, "e++++ ");  break;
      case 3: fprintf(fcode, "e+++ ");   break;
      case 4: fprintf(fcode, "e++ ");    break;
      case 5: fprintf(fcode, "e+ ");     break;
      case 6: fprintf(fcode, "e ");      break;
      case 7: fprintf(fcode, "e- ");     break;
      case 8: fprintf(fcode, "e-- ");    break;
      case 9: fprintf(fcode, "e* ");     break;
     }

   switch (gc_housing)
     {
      case 1: fprintf(fcode, "h++ ");   break;
      case 2: fprintf(fcode, "h+ ");    break;
      case 3: fprintf(fcode, "h ");     break;
      case 4: fprintf(fcode, "h- ");    break;
      case 5: fprintf(fcode, "h-- ");   break;
      case 6: fprintf(fcode, "h--- ");  break;
      case 7: fprintf(fcode, "h---- "); break;
      case 8: fprintf(fcode, "h! ");    break;
      case 9: fprintf(fcode, "h* ");    break;
     }

   switch (gc_relationships)
     {
      case  1: fprintf(fcode, "r+++ "); break;
      case  2: fprintf(fcode, "r++ ");  break;
      case  3: fprintf(fcode, "r+ ");   break;
      case  4: fprintf(fcode, "r ");    break;
      case  5: fprintf(fcode, "r- ");   break;
      case  6: fprintf(fcode, "r-- ");  break;
      case  7: fprintf(fcode, "r--- "); break;
      case  8: fprintf(fcode, "!r ");   break;
      case  9: fprintf(fcode, "r* ");   break;
      case 10: fprintf(fcode, "r%% ");  break;
     }

   /* Added in v1.2 */
   switch (x)
     {
      case  1: sex_type="x"; break;
      case  2: sex_type="y"; break;
      case  3: sex_type="z"; break;
      default: sex_type="z"; break;
     }

   /* Modified in v1.2 */
   switch (gc_sex)
     {
      case  1: fprintf(fcode, "%s+++++ ", sex_type); break;
      case  2: fprintf(fcode, "%s++++ ",  sex_type); break;
      case  3: fprintf(fcode, "%s+++ ",   sex_type); break;
      case  4: fprintf(fcode, "%s++ ",    sex_type); break;
      case  5: fprintf(fcode, "%s+ ",     sex_type); break;
      case  6: fprintf(fcode, "%s ",      sex_type); break;
      case  7: fprintf(fcode, "%s- ",     sex_type); break;
      case  8: fprintf(fcode, "%s-- ",    sex_type); break;
      case  9: fprintf(fcode, "%s--- ",   sex_type); break;
      case 10: fprintf(fcode, "%s* ",     sex_type); break;
      case 11: fprintf(fcode, "%s** ",    sex_type); break;
      case 12: fprintf(fcode, "!%s ",     sex_type); break;
      case 13: fprintf(fcode, "%s? ",     sex_type); break;
      case 14: fprintf(fcode, "!%s+ ",    sex_type); break;
     }

    fprintf(fcode, "\n");
    fprintf(fcode, "------END GEEK CODE BLOCK------\n");

    fclose(fcode);
}
